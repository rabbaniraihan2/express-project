const router = require('express').Router()
// const { postHome } = require('../../latihan-express/controller')
const { getAllUser, renderPageForChap3, renderPageForChap4, postUsers, getHomeUser } = require('../controller')
const {checkUsers} = require('../middleware')

router.get('/home', getHomeUser)
router.get('/chap3', renderPageForChap3)
router.get('/chap4', renderPageForChap4)
router.post('/users', checkUsers, postUsers)


module.exports = router