const users = require('../models/user.json')

async function getHomeUser (req,res,next) {
    try {
        const data = users['users'][0]
        res.render('home', {data : data})
    } catch (error) {
        next(error)
    }
}

async function renderPageForChap3 (req,res,next) {
    try {
        res.render('chap3')
    } catch (error) {
        next(error)
    }
}

async function renderPageForChap4 (req,res,next) {
    try {
        res.render('chap4')
    } catch (error) {
        next(error)
    }
}

async function postUsers (req,res,next) {
    try {
        const body = req.body
        if (!body.name) {
            next({
                status : 400,
                message : 'Name is required'
            })
        }else if (body.name === 'budi'){
            res.status(200).json({message : 'success'})
        }
    } catch (error) {
        next(error)
    }
}


module.exports = {
    renderPageForChap3,
    renderPageForChap4,
    postUsers,
    getHomeUser
}