const {urlencoded,json} = require('express')
const express = require('express')
const app = express()
const router = require('./routes')
const port = 4000
const {logger,errorHandler} = require('./middleware')

app.use('/static', express.static(__dirname + '/public'))
app.use(json())
app.use(urlencoded({extended:true}))
app.set('view engine', 'ejs')

//application level middleware => logging method
app.use(logger)

//route level middleware => cek di req.body itu sudah kirim password dan name atau belum
app.use(router)

//error handle middleware
app.use(errorHandler)

app.listen(port, () => {
    console.log('berhasil')
})