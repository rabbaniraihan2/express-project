function logger (req,res,next) {
    console.log(req.method, req.url)
    next()
}

function checkUsers (req,res,next) {
    try {
        const headers = req.headers
        const role = headers.role

        if(role.toLowerCase() === 'budi') {
            next()
        }else{
            res.status(401).json({
            message : 'Unautorize Access'
        })
        }
    } catch (error) {
        next(err)
    }
}

function errorHandler (err,req,res,next) {
    if(err.status) {
        res.status(err.status).json({
            message : err.message
        })
    }else{
        res.status(500).json({
            message : 'SERVER ERROR'
        })
    }
}


module.exports = {
    logger,
    errorHandler,
    checkUsers
}